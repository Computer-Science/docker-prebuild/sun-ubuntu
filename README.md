## Docker Image with base Bionic Install
This image was build from [ubuntu:bionic](https://hub.docker.com/_/ubuntu/).

The university repos have been setup here, so that users can `apt install` in later images without needing to add the repo's.  

## Usage
### Building your own image
Please use our repo at the top of your Dockerfile. e.g.
```
FROM reg.cs.sun.ac.za/computer-science/docker-prebuild/sun-ubuntu
```
### Running this image
You can run this image directly:
```
docker run -it reg.cs.sun.ac.za/computer-science/docker-prebuild/sun-ubuntu bash
```
